<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$hewan = new animals("shaun");

echo " Name : ".$hewan->name . "<br>";
echo " Legs : ".$hewan->legs . "<br>";
echo " Cold blooded : ".$hewan->cold_blooded. "<br><br>";

$frog = new frog("buduk");

echo " Name : ".$frog->name . "<br>";
echo " Legs : ".$frog->legs . "<br>";
echo " Cold blooded : ".$frog->cold_blooded. "<br>";
echo " Jump : ".$frog->jump. "<br><br>";

$ape = new ape ("Kera sakiti");

echo " Name : ".$ape->name . "<br>";
echo " Legs : ".$ape->legs . "<br>";
echo " Cold blooded : ".$ape->cold_blooded. "<br>";
echo " Jump : ".$ape->yell. "<br><br>";

?>